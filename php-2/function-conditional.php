<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>

<body>
    <h1>Berlatih Function PHP</h1>
    <?php

    echo "<h3> Soal No 1 Greetings </h3>";
    /* 
            Soal No 1
            Greetings
            Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

            contoh: greetings("abduh");
            Output: "Halo Abduh, Selamat Datang di Sanbercode!"
        */

    // Code function di sini
    function greetings($string)
    {
        echo "<p>Halo $string, Selamat Datang di Sanbercode! </p>";
    }

    //      komentar untuk menjalankan code!
    greetings("Bagas");
    greetings("Wahyu");
    greetings("Abdul");

    echo "<br>";

    echo "<h3>Soal No 2 Reverse String</h3>";
    /* 
            Soal No 2
            Reverse String
            Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping (for/while/do while).
            Function reverseString menerima satu parameter berupa string.
            NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

            reverseString("abdul");
            Output: ludba
            
        */

    // Code function di sini 
    function reverse($kata)
    {
        $length = strlen($kata);
        $tampung = "";
        for ($i = ($length - 1); $i >= 0; $i--) {
            $tampung .= $kata[$i];
        }
        return $tampung;
    }
    function reverseString($kata2)
    {
        $string = reverse($kata2);
        echo "<p>" . $string . "<p>";
    }
    // Hapus komentar di bawah ini untuk jalankan Code
    reverseString("abduh");
    reverseString("Sanbercode");
    reverseString("We Are Sanbers Developers");
    echo "<br>";

    echo "<h3>Soal No 3 Palindrome </h3>";
    /* 
            Soal No 3 
            Palindrome
            Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
            Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
            Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
            NB: 
            Contoh: 
            palindrome("katak") =>  output : true
            palindrome("jambu") => output : false
            NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!
            
        */


    // Code function di sini
    function palindrome($kata3)
    {
        $balik = reverse($kata3);
        if ($kata3 == $balik) {
            echo $kata3 . " => True <br>";
        } else {
            echo $kata3 . " => False <br>";
        }
    }
    // Hapus komentar di bawah ini untuk jalankan code
    palindrome("civic"); // true
    palindrome("nababan"); // true
    palindrome("jambaban"); // false
    palindrome("racecar"); // true

    echo "<br>";
    echo "<h3>Soal No 4 Tentukan Nilai</h3>";
    /*
        Soal 4
        buatlah sebuah function bernama tentukan-nilai . Di dalam function tentukan_nilai yang menerima parameter 
        berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
        Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar 
        sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
    */

    // Code function di sini
    function tentukan_nilai($number)
    {
        if ($number >= 85 && $number <= 100) {
            return "Sangat Baik <br>";
        } else if ($number >= 70 && $number <= 85) {
            return "Baik <br>";
        } else if ($number >= 60 && $number <= 70) {
            return "Cukup <br>";
        } else {
            return "Kurang <br>";
        }
    }
    // Hapus komentar di bawah ini untuk jalankan code
    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang

    echo "<br>";
    echo "<h3>Quis</h3>";
    
    function xo($str) {
        //Code disini
        $xstr = substr_count($str,"x");
        $ostr = substr_count($str,"o");
        // $jumlah = strlen($str);
        if ($xstr == $ostr) {
            echo "output xo => True <br>";
        } else {
            echo "output xo => False <br>";
        }
    }

    // Test Cases
    echo xo('xoxoxo'); // "Benar"
    echo xo('oxooxo'); // "Salah"
    echo xo('oxo'); // "Salah"
    echo xo('xxooox'); // "Benar"
    echo xo('xoxooxxo'); // "Benar"
    

    echo "<br>";
    echo "<h3>Quis 2</h3>";


    function pagar_bintang($integer){
        //code disini
            for($i = 1; $i <= $integer; $i++){
                for($j = 1; $j <= $integer;$j ++ ){
                    if($i % 2 == 0 ){
                        echo "*";
                    }else{
                        echo "#";
                    }
                }
                echo "<br>";
            }
            echo "<br>";
        }
        
        
        echo pagar_bintang(5);
        echo pagar_bintang(8);
        echo pagar_bintang(10);

        // echo "Asterix: ";
        // echo "<br>";
        // for ($i = 1; $i <= 5; $i++) {
        //     for ($j = 1; $j <= $i; $j++) {
        //         echo "* ";
        //     }
        //     echo "<br>";
        // }
    ?>
</body>

</html>