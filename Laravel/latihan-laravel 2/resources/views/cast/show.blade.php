@extends('layouts.master')
@section('title')
Tampil Data : <b> {{$cast->nama}}</b>
@endsection
@section('content')
<h2>Show Cast {{$cast->id}}</h2>
<h4>Nama : {{$cast->nama}}</h4>
<p> Umur : {{$cast->umur}}</p>
<p>Biodata : {{$cast->bio}}</p>
<a href="/cast" class="btn btn-info">Kembali</a>
@endsection

