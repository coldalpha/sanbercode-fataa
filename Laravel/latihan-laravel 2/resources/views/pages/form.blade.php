@extends('layouts.master')
@section('title')
Buat Account Baru!
@endsection
@section('content')
    <form action="/welcome" method="POST">
        @csrf
      <h1>Buat Account Baru!</h1>
      <h3>Sign Up Form</h3>
      <label for="firstname">First name:</label>
      <br /><br />
      <input type="text" id="firstname" name="firstname" />
      <br /><br />
      <label for="lastname">Last name:</label>
      <br /><br />
      <input type="text" id="lastname" name="lastname" />
      <br /><br />
      <label for="gender">Gender:</label>
      <br /><br />
      <input type="radio" name="gender" />Male
      <br />
      <input type="radio" name="gender" />Female
      <br />
      <input type="radio" name="gender" />Other <br /><br />
      <label for="nationality">Nationality:</label>
      <br /><br />
      <select name="nationality" id="nationality">
        <option value="1">Indonesian</option>
        <option value="2">Malaysian</option>
        <option value="3">Singapura</option>
        <option value="4">Thailand</option>
        <option value="5">Kamboja</option>
      </select>
      <br /><br />
      <label>Language Spoken</label>
      <br /><br />
      <input type="checkbox" name="language" id="bi" />Bahasa Indonesia
      <br />
      <input type="checkbox" name="language" id="en" />English
      <br />
      <input type="checkbox" name="language" id="ot" />Other
      <br />
      <br />
      <label for="bio">Bio:</label>
      <br /><br />
      <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
      <br />
      <input type="submit" value="Daftar" />
    </form>
    @endsection
